# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_07_15_064210) do
  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "admins", force: :cascade do |t|
    t.string "roles"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  end

  create_table "invoices", force: :cascade do |t|
    t.integer "user_id"
    t.integer "issuer_id", null: false
    t.float "total_price"
    t.float "tax_amount"
    t.float "product_price"
    t.string "invoice_number"
    t.date "invoice_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_invoices_on_user_id"
  end

  create_table "loan_statuses", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payment_principles", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payment_schedules", force: :cascade do |t|
    t.integer "user_loan_id"
    t.date "payment_date"
    t.float "begining_balance"
    t.float "ending_balance"
    t.float "payment_amount"
    t.float "interest"
    t.float "principle"
    t.float "penalty"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_loan_id"], name: "index_payment_schedules_on_user_loan_id"
  end

  create_table "product_invoices", force: :cascade do |t|
    t.integer "invoice_id"
    t.integer "product_id"
    t.integer "product_variant_id"
    t.string "product_name"
    t.integer "qty"
    t.float "unit_price"
    t.float "total_price"
    t.string "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["invoice_id"], name: "index_product_invoices_on_invoice_id"
    t.index ["product_id"], name: "index_product_invoices_on_product_id"
    t.index ["product_variant_id"], name: "index_product_invoices_on_product_variant_id"
  end

  create_table "product_variants", force: :cascade do |t|
    t.integer "product_id"
    t.integer "qty"
    t.string "color"
    t.string "dimension"
    t.string "weight"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "price"
    t.index ["product_id"], name: "index_product_variants_on_product_id"
  end

# Could not dump table "products" because of following StandardError
#   Unknown type 'attachments' for column 'images'

  create_table "user_loans", force: :cascade do |t|
    t.integer "user_id"
    t.integer "invoice_id"
    t.integer "loan_status_id"
    t.integer "payment_principle_id"
    t.float "down_payment"
    t.float "loan_amount"
    t.float "recieved_amount"
    t.float "interest_rate"
    t.float "total_interest"
    t.float "total_amount"
    t.date "start_date"
    t.date "due_date"
    t.integer "duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["invoice_id"], name: "index_user_loans_on_invoice_id"
    t.index ["loan_status_id"], name: "index_user_loans_on_loan_status_id"
    t.index ["payment_principle_id"], name: "index_user_loans_on_payment_principle_id"
    t.index ["user_id"], name: "index_user_loans_on_user_id"
  end

# Could not dump table "users" because of following StandardError
#   Unknown type 'attachments' for column 'profile_photo'

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "invoices", "users"
  add_foreign_key "payment_schedules", "user_loans"
  add_foreign_key "product_invoices", "invoices"
  add_foreign_key "product_invoices", "product_variants"
  add_foreign_key "product_invoices", "products"
  add_foreign_key "product_variants", "products"
  add_foreign_key "user_loans", "invoices"
  add_foreign_key "user_loans", "loan_statuses"
  add_foreign_key "user_loans", "payment_principles"
  add_foreign_key "user_loans", "users"
end
