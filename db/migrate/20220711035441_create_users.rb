class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :phone_number
      t.date   :date_of_birth
      t.string :gender
      t.string :profile_photo
      t.string :images
      t.string :address

      t.timestamps
    end
  end
end
