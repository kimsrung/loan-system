class CreatePaymentTerms < ActiveRecord::Migration[7.0]
  def change
    create_table :payment_terms do |t|
      t.string :name
      t.string :description
      t.timestamps
    end
  end
end
