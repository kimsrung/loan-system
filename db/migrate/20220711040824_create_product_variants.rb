class CreateProductVariants < ActiveRecord::Migration[7.0]
  def change
    create_table :product_variants do |t|
      t.belongs_to :product, foreign_key: true
      t.integer   :qty
      t.string    :color
      t.string    :dimension
      t.string    :weight

      t.timestamps
    end
  end
end
