class RenameTotalPaymentToPaymentAmountInPaymentSchedules < ActiveRecord::Migration[7.0]
  def change
    rename_column :payment_schedules, :total_payment, :payment_amount
  end
end
