class ChangeColumnTypeImagesAndProfilePhotoInUsers < ActiveRecord::Migration[7.0]
  def change
    change_column :users, :images, :attachments
    change_column :users, :profile_photo, :attachments
  end
end
