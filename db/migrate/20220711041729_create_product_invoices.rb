class CreateProductInvoices < ActiveRecord::Migration[7.0]
  def change
    create_table :product_invoices do |t|
      t.belongs_to :invoice, foreign_key: true
      t.belongs_to :product, foreign_key: true
      t.belongs_to :product_variant, foreign_key: true
      t.string    :product_name
      t.integer   :qty
      t.float     :unit_price
      t.float     :total_price
      t.string    :color

      t.timestamps
    end
  end
end
