class RenamePaymentTermsToPaymentPrinciples < ActiveRecord::Migration[7.0]
  def change
    rename_table :payment_terms, :payment_principles
  end
end
