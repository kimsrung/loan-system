class CreateInvoices < ActiveRecord::Migration[7.0]
  def change
    create_table :invoices do |t|
      t.references :user, foreign_key: true
      t.integer   :issuer_id, null: false
      t.float     :total_price
      t.float     :tax_amount
      t.float     :product_price
      t.string    :invoice_number
      t.date      :invoice_date

      t.timestamps
    end
  end
end
