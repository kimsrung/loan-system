class CreatePaymentSchedules < ActiveRecord::Migration[7.0]
  def change
    create_table :payment_schedules do |t|
      t.belongs_to :user_loan, foreign_key: true
      t.date  :payment_date
      t.float :begining_balance
      t.float :ending_balance
      t.float :total_payment
      t.float :interest
      t.float :principle
      t.float :penalty

      t.timestamps
    end
  end
end
