class ChangeColumnTypeImagesInProducts < ActiveRecord::Migration[7.0]
  def change
    change_column :products, :images, :attachments
  end
end
