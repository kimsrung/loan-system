class RenamePaymentTermIdToPaymentPrincipleIdInUserLoan < ActiveRecord::Migration[7.0]
  def change
    rename_column :user_loans, :payment_term_id, :payment_principle_id
    rename_index :user_loans, 'index_user_loans_on_payment_term_id', 'index_user_loans_on_payment_principle_id'
  end
end
