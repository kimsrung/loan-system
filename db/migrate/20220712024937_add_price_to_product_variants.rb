class AddPriceToProductVariants < ActiveRecord::Migration[7.0]
  def change
    add_column :product_variants, :price, :float
  end
end
