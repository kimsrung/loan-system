class CreateUserLoans < ActiveRecord::Migration[7.0]
  def change
    create_table :user_loans do |t|
      t.belongs_to :user, foreign_key: true
      t.belongs_to :invoice, foreign_key: true
      t.belongs_to :loan_status, foreign_key: true
      t.belongs_to :payment_term, foreign_key: true
      t.float     :down_payment
      t.float     :loan_amount
      t.float     :recieved_amount
      t.float     :interest_rate
      t.float     :total_interest
      t.float     :total_amount
      t.date      :start_date
      t.date      :due_date
      t.integer   :duration

      t.timestamps
    end
  end
end
