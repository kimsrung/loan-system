# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  description :string
#  images      :attachments
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
require 'test_helper'

class ProductTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "should not save product without name" do
    product = Product.new
    assert_not product.save
  end
end
