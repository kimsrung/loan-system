# == Schema Information
#
# Table name: user_loans
#
#  id                   :integer          not null, primary key
#  down_payment         :float
#  due_date             :date
#  duration             :integer
#  interest_rate        :float
#  loan_amount          :float
#  recieved_amount      :float
#  start_date           :date
#  total_amount         :float
#  total_interest       :float
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  invoice_id           :integer
#  loan_status_id       :integer
#  payment_principle_id :integer
#  user_id              :integer
#
# Indexes
#
#  index_user_loans_on_invoice_id            (invoice_id)
#  index_user_loans_on_loan_status_id        (loan_status_id)
#  index_user_loans_on_payment_principle_id  (payment_principle_id)
#  index_user_loans_on_user_id               (user_id)
#
# Foreign Keys
#
#  invoice_id            (invoice_id => invoices.id)
#  loan_status_id        (loan_status_id => loan_statuses.id)
#  payment_principle_id  (payment_principle_id => payment_principles.id)
#  user_id               (user_id => users.id)
#
require 'test_helper'

class UserLoanTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
