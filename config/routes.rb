Rails.application.routes.draw do
  devise_for :admins, skip: :registration
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  get 'home/index'
  root to: 'home#index'

  resources :users
  resources :user_loans
  resources :products
  resources :product_variants
  resources :payment_schedules
  resources :invoices
end
