class InvoicesController < ApplicationController
  def index
    @invoices = Invoice.order(:id).page params[:page]
  end

  def show
    @invoice = Invoice.find(params[:id])
  end
end
