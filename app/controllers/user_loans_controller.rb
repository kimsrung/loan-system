class UserLoansController < ApplicationController
  def index
    @loans = UserLoan.order(:id).page params[:page]
  end

  def show
    @loan = UserLoan.find(params[:id])
    @invoice = @loan.invoice
    @payment_schedules = @loan.payment_schedules.order(:id).page params[:page]
  end
end
