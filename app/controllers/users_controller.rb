class UsersController < ApplicationController
  before_action :set_user, only: %i[show edit update destroy]

  def index
    @users = User.order(:id).page params[:page]
  end

  # rubocop:disable Metrics/MethodLength
  def show; end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      redirect_to @user
    else
      render :new, status: :unprocessable_entity
    end
  end

  # rubocop:disable Metrics/MethodLength
  def edit; end

  def update
    if @user.update(user_params)
      redirect_to @user
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @user.destroy

    redirect_to users_path, status: :ok
  end

  private

  def user_params
    params.require(:user).permit(:name, :address, :date_of_birth, :gender,
                                 :phone_number, :profile_photo, images: [])
  end

  def set_user
    @user = User.find(params[:id])
  end
end
