class HomeController < ApplicationController
  before_action :authenticate_admin!

  # rubocop:disable Metrics/MethodLength
  def index; end
end
