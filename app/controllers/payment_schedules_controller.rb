class PaymentSchedulesController < ApplicationController
  def index
    @loan = UserLoan.find(params[:loan_id])
    @payment_schedules = @loan.payment_schedules.order(:id).page params[:page]
  end
end
