class ProductVariantsController < ApplicationController
  before_action :set_variant, only: %i[show destroy]

  # rubocop:disable Metrics/MethodLength
  def show; end

  private

  def set_variant
    @variant = ProductVariant.find(params[:id])
  end
end
