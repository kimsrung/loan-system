// app/javascripts/controllers/upload_controller.js

import { Controller } from "@hotwired/stimulus";

export default class extends Controller {
  static targets = ["input", "progress"];

  uploadFile() {
    Array.from(this.inputTarget.files).forEach((file) => {
      let reader = new FileReader();
      reader.onload = function (e) {
        document.querySelector('#img-preview').setAttribute('src', e.target.result);
        document.querySelector('#img-preview').width = 150;
        document.querySelector('#img-preview').height = 200
      };

      reader.readAsDataURL(file);
    });
  }

  uploadFiles() {
    Array.from(this.inputTarget.files).forEach((file) => {
      let reader = new FileReader();
      reader.onload = function (e) {
        document.querySelector('#imgs-preview').setAttribute('src', e.target.result);
        document.querySelector('#imgs-preview').width = 150;
        document.querySelector('#imgs-preview').height = 200
      };

      reader.readAsDataURL(file);
    });
  }
}