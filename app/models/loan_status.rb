# == Schema Information
#
# Table name: loan_statuses
#
#  id          :integer          not null, primary key
#  description :string
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class LoanStatus < ApplicationRecord
end
