# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  description :string
#  images      :attachments
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class Product < ApplicationRecord
  has_many :variants, class_name: 'ProductVariant', dependent: :destroy
  has_many_attached :images, dependent: :destroy
  accepts_nested_attributes_for :variants, allow_destroy: true

  validates :name, presence: true
  validates :description, length: { minimum: 200 }
  validates :images, presence: true
end
