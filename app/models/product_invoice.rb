# == Schema Information
#
# Table name: product_invoices
#
#  id                 :integer          not null, primary key
#  color              :string
#  product_name       :string
#  qty                :integer
#  total_price        :float
#  unit_price         :float
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  invoice_id         :integer
#  product_id         :integer
#  product_variant_id :integer
#
# Indexes
#
#  index_product_invoices_on_invoice_id          (invoice_id)
#  index_product_invoices_on_product_id          (product_id)
#  index_product_invoices_on_product_variant_id  (product_variant_id)
#
# Foreign Keys
#
#  invoice_id          (invoice_id => invoices.id)
#  product_id          (product_id => products.id)
#  product_variant_id  (product_variant_id => product_variants.id)
#
class ProductInvoice < ApplicationRecord
  belongs_to :invoice
  belongs_to :product
  belongs_to :product_variant
end
