# == Schema Information
#
# Table name: payment_principles
#
#  id          :integer          not null, primary key
#  description :string
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class PaymentPrinciple < ApplicationRecord
end
