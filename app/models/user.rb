# == Schema Information
#
# Table name: users
#
#  id            :integer          not null, primary key
#  address       :string
#  date_of_birth :date
#  gender        :string
#  images        :
#  name          :string
#  phone_number  :string
#  profile_photo :attachments
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
class User < ApplicationRecord
  paginates_per 20
  has_one_attached :profile_photo, dependent: :destroy
  has_many_attached :images, dependent: :destroy
end
