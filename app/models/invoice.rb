# == Schema Information
#
# Table name: invoices
#
#  id             :integer          not null, primary key
#  invoice_date   :date
#  invoice_number :string
#  product_price  :float
#  tax_amount     :float
#  total_price    :float
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  issuer_id      :integer          not null
#  user_id        :integer
#
# Indexes
#
#  index_invoices_on_user_id  (user_id)
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Invoice < ApplicationRecord
  has_many :products, class_name: 'ProductInvoice'
  belongs_to  :buyer, class_name: 'User', foreign_key: 'user_id'
  belongs_to  :issuer, class_name: 'Admin', foreign_key: 'issuer_id'
end
